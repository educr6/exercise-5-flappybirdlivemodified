﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPull : MonoBehaviour
{
    public int columnPoolSize = 3;
    public GameObject columnPrefab;
    public GameObject columnNarrowPrefab;
    public GameObject columnWidePrefab;
    public float spawnRate = 4f;
    public float columnMin = -2;
    public float columnMax = 0.8f;

    private GameObject[] columns;
    private Vector2 objectPoolPosition = new Vector2(-15f, -25);
    private float timeSinceLastSpawned;
    private float spawnXPosition = 10f;
    private int currentColumn = 0;
    private bool isIncreasing = true;
    


    private void Start()
    {
        columns = new GameObject[columnPoolSize];



            
                columns[0] = (GameObject)Instantiate(columnNarrowPrefab, objectPoolPosition, Quaternion.identity);
                columns[1] = (GameObject)Instantiate(columnPrefab, objectPoolPosition, Quaternion.identity);
                columns[2] = (GameObject)Instantiate(columnWidePrefab, objectPoolPosition, Quaternion.identity);



    }

    private void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;

        if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0;
            float spawnYPosition = Random.Range(columnMin, columnMax);
            columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);

            if (isIncreasing)
            {
                currentColumn++;
                if (currentColumn >= columnPoolSize)
                {

                    currentColumn = 0;
                }

            }
          

           

        }


    }



}
